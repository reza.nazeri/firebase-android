package ir.partsilicon.inbox;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

/***
 * Created by reza on 6/13/17.
 ***/

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(Constant.TAG, "From: " + remoteMessage.getFrom());
        Log.d(Constant.TAG, "context package: " + getPackageName());
        DataManager.init(this);

        // Check if message contains a data payload.
        final Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.d(Constant.TAG, "Message data payload: " + data);
//            new Handler(Looper.getMainLooper()).post(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(getApplicationContext(), "payload: " + data, Toast.LENGTH_SHORT).show();
//                }
//            });

            DataManager.saveServerUrl(data.get("serverUrl"));
            if (data.containsKey("type")) {
                // TODO: 7/11/17 check this message is in user favCategory default is all cat
                switch (data.get("type")) {
                    case Constant.TYPE_NEW: {
                        handleNewMessage(data);
                        break;
                    }
                    case Constant.TYPE_UPDATE: {
                        Message message = EasyHTTP.getMessage(data.get("url"), DataManager.getPackageName());
                        DataManager.updateMessage(message);
                        Log.d(Constant.TAG, "message updated");
                        break;
                    }
                    case Constant.TYPE_CATEGORY: {
                        handleCategoryMessage(data);
                        break;
                    }
                }
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(Constant.TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        Log.d(Constant.TAG, "onMessageReceived: finish");
    }

    private void handleNewMessage(Map<String, String> data) {
        List<Message> messages = EasyHTTP.getMessages(data.get("serverUrl") + data.get("method") + Message.getLastId(), DataManager.getPackageName());
        DataManager.saveMessage(messages);
        Message m = null;
        for (Message message : messages) {
            if (message.getNotification() == Message.NOTIFICATION) {
                m = message;
            }
        }

        if (m != null) {
            NotificationUtils notification = new NotificationUtils(this);
            Intent intent = new Intent(this, InboxDetailActivity.class);
            intent.putExtra(InboxDetailActivity.MESSAGE, m);
            if (data.containsKey("notifImage")) {
                notification.showNotificationMessage(m.getTitle(), m.getContent(), null, intent, data.get("notifImage"));
            } else {
                notification.showNotificationMessage(m.getTitle(), m.getContent(), null, intent);
            }
        }
    }

    private void handleCategoryMessage(Map<String, String> data) {
        List<Category> list = EasyHTTP.getCategory(data.get("serverUrl") + data.get("method"), DataManager.getPackageName());
        DataManager.saveCategory(list);
        Log.d(Constant.TAG, "category updated");
    }

//    private void showPopup(final String imageUrl, final String link) {
//        Log.d(Constant.TAG, "showPopup: " + imageUrl);
//        final Dialog alert = new Dialog(this);
//        alert.setContentView(R.layout.popup_dialog_layout);
//        alert.setCancelable(true);
//        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
//
//        alert.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                ImageView imageView = (ImageView) alert.findViewById(R.id.popup_imageView);
//                assert imageView != null;
//                Glide.with(MyFirebaseMessagingService.this).load(Constant.SERVER_URL + imageUrl)
//                        .fitCenter()
//                        .centerCrop()
//                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                        .skipMemoryCache(true)
//                        .listener(new RequestListener<String, GlideDrawable>() {
//                            @Override
//                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
//                                alert.dismiss();
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
//                                return false;
//                            }
//                        })
//                        .into(imageView);
//
//                imageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
//                    }
//                });
//            }
//        });
//
//        alert.show();
//    }


//    private void showPopup(final String imageUrl, final String link) {
//        Log.d(Constant.TAG, "showPopup: " + imageUrl);
//        final AlertDialog alert = new AlertDialog.Builder(InboxManager.context, R.style.PopupThem)
//                .setView(R.layout.popup_dialog_layout)
////                .setMessage("تعداد ماژول های تابلو  را وارد کنید")
////                .setTitle("تعداد ماژول")
//                .create();
//
//        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        alert.setButton(AlertDialog.BUTTON_POSITIVE, "رد کردن", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        alert.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                ImageView imageView = (ImageView) alert.findViewById(R.id.popup_imageView);
//                assert imageView != null;
//                Glide.with(MyFirebaseMessagingService.this).load(Constant.SERVER_URL + imageUrl)
//                        .fitCenter()
//                        .centerCrop()
//                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                        .skipMemoryCache(true)
//                        .listener(new RequestListener<String, GlideDrawable>() {
//                            @Override
//                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
//                                alert.dismiss();
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
//                                return false;
//                            }
//                        })
//                        .into(imageView);
//
//                imageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
//                    }
//                });
//            }
//        });
//
//        alert.show();
//    }
}
