package ir.partsilicon.inbox;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/***
 * Created by reza on 6/18/17.
 ***/

@Table(name = "Message", id = "_id")
public class Message extends Model implements Parcelable {

    public static final int NONE = 0;
    public static final int NOTIFICATION = 1;
    public static final int POPUP = 2;

    public static final int FLAG_OK = 1;
    public static final int FLAG_PENDING = 2;

    //@SerializedName("id")
    @Column(name = "id")
    public long msgId;

    //@SerializedName("title")
    @Column(name = "title")
    public String title;

    //@SerializedName("content")
    @Column(name = "content")
    public String content;

    //@SerializedName("image")
    @Column(name = "image")
    public String image;

    //@SerializedName("link")
    @Column(name = "link")
    public String link;

    //@SerializedName("notification")
    @Column(name = "notification")
    public int notification;

    //@SerializedName("type")
    @Column(name = "type")
    public int type;

//    //@SerializedName("time")
//    @Column(name = "time")
//    public String time;

    //@SerializedName("category")
    @Column(name = "category")
    public long category;

    //@SerializedName("visit")
    @Column(name = "visit")
    public int visit;

//    //@SerializedName("deliver")
//    @Column(name = "deliver")
//    public int deliver;

    @Column(name = "createdAt")
    public String createdAt;

    @Column(name = "open")
    public int open;

    @Column(name = "flag")
    public int flag = FLAG_OK;

    public Message() {
        super();
    }

//    public Message(long catId, String title, String content, String image, String link, int notification, int type, String time, int category, int visit, int deliver, boolean open) {
//        super();
//        this.catId = catId;
//        this.title = title;
//        this.content = content;
//        this.image = image;
//        this.link = link;
//        this.notification = notification;
//        this.type = type;
//        this.time = time;
//        this.category = category;
//        this.visit = visit;
//        this.deliver = deliver;
//        this.open = open;
//    }

    public Message(JSONObject json) {
        super();
        try {
            if (json.has("id")) {
                this.msgId = json.getLong("id");
            }
            if (json.has("title")) {
                this.title = json.getString("title");
            }
            if (json.has("content")) {
                this.content = json.getString("content");
            }
            if (json.has("image")) {
                this.image = json.getString("image");
            }
            if (json.has("link")) {
                this.link = json.getString("link");
            }
            if (json.has("notification")) {
                this.notification = json.getInt("notification");
            }
            if (json.has("type")) {
                this.type = json.getInt("type");
            }
//            if (json.has("time")) {
//                this.time = json.getString("time");
//            }
            if (json.has("category")) {
                this.category = json.getLong("category");
            }
            if (json.has("visit")) {
                this.visit = json.getInt("visit");
            }
//            if (json.has("deliver")) {
//                this.deliver = json.getInt("deliver");
//            }
            if (json.has("createdAt")) {
                this.createdAt = json.getString("createdAt");
            }
            open = 0;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public static List<Message> getAll() {
//        List<Contact> contact = null;
//        try {
//            contact = getMany(Contact.class, "Contact");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return contact;
        List<Message> list = null;
        try {
            list = new Select()
                    .from(Message.class)
                    .orderBy("id DESC")
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Nullable
    public static Message getLast() {
        try {
            return new Select()
                    .from(Message.class)
                    .orderBy("id DESC")
                    .limit(1)
                    .executeSingle();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getLastId() {
        try {
            return getLast().getMsgId();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Message getLastPopup() {
        try {
            return new Select()
                    .from(Message.class)
                    .where("notification = ? AND open = ?", POPUP, 0)
                    .orderBy("id DESC")
                    .executeSingle();
        } catch (Exception e) {
            Log.d(Constant.TAG, "getLastPopup: error:", e);
            return null;
        }
    }

    public static int getUnreadCount() {
        try {
            return new Select()
                    .from(Message.class)
                    .where("open = ?", 0)
                    .execute().size();
        } catch (Exception e) {
            Log.d(Constant.TAG, "getLastPopup: error:", e);
            return 0;
        }
    }

    public void setOpenLastPopup() {
        try {
            new Update(Message.class)
                    .set("open = ?", true)
                    .where("id <= ? AND notification = ?", msgId, POPUP)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateVisit(int visit, long msgId) {
        try {
            new Update(Message.class)
                    .set("visit = ?", visit)
                    .where("id = ?", msgId)
                    .execute();
//            Log.d(Constant.TAG, "message " + msgId + " set visit: " + visit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateOpen(int open) {
        try {
            new Update(Message.class)
                    .set("open = ?", open)
                    .where("id = ?", this.msgId)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateOpen(int open, long msgId) {
        try {
            new Update(Message.class)
                    .set("open = ?", open)
                    .where("id = ?", msgId)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateFlag(int flag) {
        try {
            new Update(Message.class)
                    .set("flag = ?", flag)
                    .where("id = ?", this.msgId)
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getMsgId() {
        return msgId;
    }

    public void setMsgId(long msgId) {
        this.msgId = msgId;
    }

    public String getimage() {
        return image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getNotification() {
        return notification;
    }

    public void setNotification(int notification) {
        this.notification = notification;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

//    public String getTime() {
//        return time;
//    }

//    public void setTime(String time) {
//        this.time = time;
//    }

    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public int getVisit() {
        return visit;
    }

    public void setVisit(int visit) {
        this.visit = visit;
    }

//    public int getDeliver() {
//        return deliver;
//    }

//    public void setDeliver(int deliver) {
//        this.deliver = deliver;
//    }

    /**
     * 0: false
     * 1: true
     **/
    public int getOpen() {
        return open;
    }

    /**
     * 0: false
     * 1: true
     **/
    public void setOpen(int open) {
        this.open = open;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.msgId);
        dest.writeString(this.title);
        dest.writeString(this.content);
        dest.writeString(this.image);
        dest.writeString(this.link);
        dest.writeInt(this.notification);
        dest.writeInt(this.type);
        dest.writeLong(this.category);
        dest.writeInt(this.visit);
        dest.writeString(this.createdAt);
        dest.writeInt(this.open);
        dest.writeInt(this.flag);
    }

    protected Message(Parcel in) {
        this.msgId = in.readLong();
        this.title = in.readString();
        this.content = in.readString();
        this.image = in.readString();
        this.link = in.readString();
        this.notification = in.readInt();
        this.type = in.readInt();
        this.category = in.readLong();
        this.visit = in.readInt();
        this.createdAt = in.readString();
        this.open = in.readInt();
        this.flag = in.readInt();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
