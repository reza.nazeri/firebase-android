package ir.partsilicon.inbox;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/***
 * Created by reza on 6/18/17.
 ***/

class EasyHTTP {

    //    private static OkHttpClient client = new OkHttpClient();
    private static OkHttpClient client = new OkHttpClient()
            .newBuilder()
            .connectTimeout(8000, TimeUnit.MILLISECONDS)
            .readTimeout(8000, TimeUnit.MILLISECONDS)
            .writeTimeout(8000, TimeUnit.MILLISECONDS)
            .build();

    static List<Message> getMessages(String url, String packageName) {
        url = addArgs(url, packageName);
        Log.d("zzz", "url: " + url);

        List<Message> list = new LinkedList<>();
        try {
            String result = get(url);
            if (result != null && !result.isEmpty()) {
                JSONArray jsonArray = new JSONArray(result);
//                Gson gson = new GsonBuilder().create();
                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(new Message(jsonArray.getJSONObject(i)));
//                    list.add(gson.fromJson(jsonArray.getString(i), Message.class));
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    static Message getMessage(String url, String packageName) {
        url = addArgs(url, packageName);
        Message message = null;
        try {
            String result = get(url);
            if (result != null && !result.isEmpty()) {
                message = new Gson().fromJson(result, Message.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }

    public static List<Category> getCategory(String url, String packageName) {
        url = addArgs(url, packageName);
        Log.d(Constant.TAG, "getCategory url: " + url);
        List<Category> list = new LinkedList<>();
        try {
            String result = get(url);
            if (result != null && !result.isEmpty()) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    list.add(new Category(jsonArray.getJSONObject(i)));
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void visit(long id, String url, final Handler.Callback callback) {
        Request request = new Request.Builder()
                .url(url + id)
                .build();

        Callback cb = new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful() && response.code() == 200) {
                    try {
                        if (callback != null) {
                            callback.handleMessage(null);
                        }
                        String result = response.body().string();
                        if (result != null && !result.isEmpty()) {
                            JSONArray jsonArray = new JSONArray(result);
                            Message message = new Message();
                            ActiveAndroid.beginTransaction();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                message.updateVisit(jsonObject.getInt("visit"), jsonObject.getLong("id"));
                            }
                            ActiveAndroid.setTransactionSuccessful();
                            ActiveAndroid.endTransaction();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        client.newCall(request).enqueue(cb);
    }

    public static boolean getVisit(String url, String packageName) {
        try {
            url = addArgs(url, packageName);
            String result = get(url);
            if (result != null && !result.isEmpty()) {
                JSONArray jsonArray = new JSONArray(result);
                Message message = new Message();
                ActiveAndroid.beginTransaction();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    message.updateVisit(jsonObject.getInt("visit"), jsonObject.getLong("id"));
                }
                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();
                return true;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Nullable
    private static String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
            return response.body().string();
        } else {
            return null;
        }
    }

    @NonNull
    private static String addArgs(String url, String packageName) {
        if (url != null && url.length() > 0 && url.charAt(url.length() - 1) == '/') {
            url = url.substring(0, url.length() - 1);
        }
        url =  url + "?p=" + packageName;
        return url;
    }

    public static boolean haveInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
