package ir.partsilicon.inbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.LinkedList;
import java.util.List;

/***
 * Created by reza nazeri on 96/4/11.
 ***/
class NavListAdapter extends RecyclerView.Adapter<NavListAdapter.ViewHolder> {

    //    private View.OnClickListener clickListener;
    private List<?> list = new LinkedList<>();
    private int rowLayout;

    public NavListAdapter(int rowLayout) {
//        this.clickListener = clickListener;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
//        view.setOnClickListener(clickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            Category data = (Category) list.get(position);
            holder.title.setText(data.getTitle());
            String imageUrl = data.getimage();
            if (imageUrl != null && !imageUrl.isEmpty()) {
//                Picasso.with(holder.image.getContext())
//                        .load(DataManager.getServerUrl() + imageUrl)
//                        .priority(Picasso.Priority.LOW)
//                        .into(holder.image);

                Glide.with(holder.image.getContext())
                        .load(DataManager.getServerUrl() + imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
                        .into(holder.image);
            }
            if (position == 0 && data.getTitle().equals("همه")) {
                byte[] decodedString = Base64.decode(Constant.cat_all, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.image.setImageBitmap(decodedByte);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<?> list) {
        this.list = list != null ? list : new LinkedList<>();
        notifyDataSetChanged();
    }

    public List<?> getData() {
        return list;
    }

//    public void setData(List<Zekr> list) {
//        this.list = list;
//        notifyDataSetChanged();
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public TextView title;
        public ImageView image;

        public ViewHolder(View view) {
            super(view);
            context = view.getContext();
            title = (TextView) view.findViewById(R.id.inbox_list_textView);
            image = (ImageView) view.findViewById(R.id.inbox_list_image);

            Typeface font = Typeface.createFromAsset(context.getAssets(), "font/iransans.ttf");
            title.setTypeface(font);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InboxActivity activity = (InboxActivity) context;
                    activity.onNavItemClickListener(getAdapterPosition());
                }
            });
        }
    }
}
