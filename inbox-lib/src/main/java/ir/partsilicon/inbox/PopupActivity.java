package ir.partsilicon.inbox;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PopupActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        ImageView imageView = (ImageView) findViewById(R.id.popup_image);
        Button button = (Button) findViewById(R.id.popup_skip);

        Typeface font = Typeface.createFromAsset(getAssets(), "font/iransans.ttf");
        button.setTypeface(font);
        final Message message = getIntent().getParcelableExtra("message");

        String url = DataManager.getServerUrl() + message.getimage();
        Log.d(Constant.TAG, "popup image url: " + url);
        Picasso.with(this).load(url)
//                .fitCenter()
//                .centerCrop()
                .noFade()
                .into(imageView);

//        Glide.with(this).load(url)
////                .fitCenter()
////                .centerCrop()
//                .dontAnimate()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .skipMemoryCache(true)
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
//                        e.printStackTrace();
//                        Toast.makeText(PopupActivity.this, "error on load image", Toast.LENGTH_SHORT).show();
////                        finish();
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
//                        return false;
//                    }
//                })
//                .into(imageView);

        EasyHTTP.visit(message.getMsgId(), DataManager.getServerUrl() + "api/v1/visit/", null);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(message.getLink())));
            }
        });
    }

//    private void showPopup(final Context context, final String imageUrl, final String link) {
//        Log.d(Constant.TAG, "showPopup: " + imageUrl);
//        final AlertDialog alert = new AlertDialog.Builder(InboxManager.context, R.style.PopupThem)
//                .setView(R.layout.popup_dialog_layout)
////                .setMessage("تعداد ماژول های تابلو  را وارد کنید")
////                .setTitle("تعداد ماژول")
//                .create();
//
////        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        alert.setButton(AlertDialog.BUTTON_POSITIVE, "رد کردن", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//
//        alert.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialog) {
//                ImageView imageView = (ImageView) alert.findViewById(R.id.popup_imageView);
//                assert imageView != null;
//                Glide.with(context).load(Constant.SERVER_URL + imageUrl)
//                        .fitCenter()
//                        .centerCrop()
//                        .diskCacheStrategy(DiskCacheStrategy.RESULT)
//                        .skipMemoryCache(true)
//                        .listener(new RequestListener<String, GlideDrawable>() {
//                            @Override
//                            public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
//                                alert.dismiss();
//                                return false;
//                            }
//
//                            @Override
//                            public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
//                                return false;
//                            }
//                        })
//                        .into(imageView);
//
//                imageView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
//                    }
//                });
//            }
//        });
//
//        alert.show();
//    }
}
