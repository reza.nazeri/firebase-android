package ir.partsilicon.inbox;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;

/***
 * Created by reza on 8/1/17.
 ***/

class BaseActivity extends AppCompatActivity {

    public void changeStatusbarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            StatusBarUtil.setColor(this, color, 0);
        }

    }
}
